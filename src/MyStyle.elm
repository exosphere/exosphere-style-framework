module MyStyle exposing (configuration)

{-|


# Functions

@docs configuration

-}

import Dict


{-| Configuration contain the variable that will be replaced in the framework.
-}
configuration : Dict.Dict String String
configuration =
    Dict.fromList
        [ ( "font_url", "https://exosphere.gitlab.io/exosphere/fonts/open-sans-regular-400.css" )
        , ( "font_typeface", "Open Sans" )
        , ( "font_fallback", "sans-serif" )
        ]
