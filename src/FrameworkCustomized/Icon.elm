module FrameworkCustomized.Icon exposing (bell, bell_, introspection)

import Color
import Element
import Framework.Color exposing (black)
import Html
import Svg
import Svg.Attributes as SA


introspection :
    { name : String
    , description : String
    , signature : String
    , variations : List ( String, List ( Element.Element msg1, String ) )
    }
introspection =
    { name = "Custom Icons"
    , description = "List of customized SVG icons"
    , signature = "Color.Color -> Int -> Element.Element msg"
    , variations =
        [ ( "Icons"
          , [ ( bell black 32, "bell black 32" )
            ]
          )
        ]
    }


bell : Color.Color -> Int -> Element.Element msg
bell cl size =
    Element.el [] <|
        Element.html <|
            bell_ cl size


bell_ : Color.Color -> Int -> Html.Html msg
bell_ cl size =
    Svg.svg
        [ SA.viewBox "0 0 500 500"
        , SA.height <| String.fromInt size
        ]
        [ Svg.path
            [ SA.fill (Color.colorToHex cl)
            , SA.d "M224 512c35.32 0 63.97-28.65 63.97-64H160.03c0 35.35 28.65 64 63.97 64zm215.39-149.71c-19.32-20.76-55.47-51.99-55.47-154.29 0-77.7-54.48-139.9-127.94-155.16V32c0-17.67-14.32-32-31.98-32s-31.98 14.33-31.98 32v20.84C118.56 68.1 64.08 130.3 64.08 208c0 102.3-36.15 133.53-55.47 154.29-6 6.45-8.66 14.16-8.61 21.71.11 16.4 12.98 32 32.1 32h383.8c19.12 0 32-15.6 32.1-32 .05-7.55-2.61-15.27-8.61-21.71z"
            ]
            []
        ]
