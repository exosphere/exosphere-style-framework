module FrameworkCustomized.Logo exposing (Logo(..), Color(..), LogoType(..), logo, introspection)

{-| [Demo](https://lucamug.github.io/elm-style-framework/#/framework/Logos/Logos)


# Functions

@docs Logo, Color, LogoType, logo, introspection

-}

import Element
import Html exposing (Html)
import Svg
import Svg.Attributes as SA


{-| -}
introspection :
    { name : String
    , description : String
    , signature : String
    , variations : List ( String, List ( Element.Element msg1, String ) )
    }
introspection =
    { name = "Custom Logos"
    , description = "List of customized SVG logos"
    , signature = ""
    , variations =
        [ ( "Logos"
          , [ ( logo (LogoExosphere Default) 100, "logo (LogoExosphere Default) 100" )
            , ( logo (LogoExosphere Monochrome) 100, "logo (LogoExosphere Monochrome) 100" )
            ]
          )
        ]
    }


{-| SVG Logo
-}
logo : Logo -> Int -> Element.Element msg
logo lg size =
    Element.el [] <|
        Element.html <|
            case lg of
                LogoExosphere logoType ->
                    logoExosphere logoType size


{-| Type of logos
-}
type Logo
    = LogoExosphere LogoType


{-| -}
type LogoType
    = Default
    | Monochrome


type alias Size =
    Int


{-| -}
type Color
    = Orange
    | Green
    | LightBlue
    | Blue
    | White
    | Gray
    | Black
    | LightTeal
    | DarkTeal


ratio : Float
ratio =
    -- Width / Height
    1


cssRgb : Color -> String
cssRgb color =
    case color of
        Orange ->
            "#f0ad00"

        Green ->
            "#7fd13b"

        LightBlue ->
            "#60b5cc"

        Blue ->
            "#5a6378"

        White ->
            "#fff"

        Gray ->
            "#666"

        Black ->
            "#000"

        LightTeal ->
            "#60EFFF"

        DarkTeal ->
            "#1F89C7"


logoExosphere : LogoType -> Size -> Html msg
logoExosphere type_ height =
    let
        exoLogoRatio =
            37.0 / 35.0

        ( colorTop, colorBottom, gradientId ) =
            case type_ of
                Default ->
                    ( cssRgb LightTeal, cssRgb DarkTeal, "SVGID_1_Default" )

                Monochrome ->
                    ( cssRgb White, cssRgb Gray, "SVGID_1_Monochrome" )

        gradientUrl =
            "url(#" ++ gradientId ++ ")"
    in
    Svg.svg
        [ SA.viewBox "0 0 37 35"
        , SA.height <| String.fromInt height
        , SA.width <| String.fromInt <| floor <| toFloat height * exoLogoRatio
        ]
        [ Svg.g []
            [ Svg.g []
                [ Svg.rect
                    [ SA.stroke gradientUrl
                    , SA.height "33"
                    , SA.x "1"
                    , SA.id "rect1"
                    , SA.strokeWidth "2"
                    , SA.width "35"
                    , SA.y "1"
                    , SA.fill "none"
                    , SA.transform ""
                    ]
                    []
                , Svg.line
                    [ SA.stroke gradientUrl
                    , SA.y1 "8.062500px"
                    , SA.strokeWidth "2"
                    , SA.x1 "1.000000px"
                    , SA.id "line1"
                    , SA.y2 "8.062500px"
                    , SA.x2 "36.000000px"
                    , SA.transform ""
                    ]
                    []
                , Svg.line
                    [ SA.stroke gradientUrl
                    , SA.y1 "1.000000"
                    , SA.id "line2"
                    , SA.strokeWidth "2"
                    , SA.x1 "26.000000"
                    , SA.y2 "8.000000"
                    , SA.x2 "26.000000"
                    , SA.transform ""
                    ]
                    []
                , Svg.g []
                    [ Svg.ellipse
                        [ SA.stroke gradientUrl
                        , SA.transform ""
                        , SA.id "ellipse1"
                        , SA.cy "20"
                        , SA.strokeWidth "3"
                        , SA.fill gradientUrl
                        , SA.rx "5"
                        , SA.cx "22"
                        , SA.ry "5"
                        ]
                        []
                    , Svg.ellipse
                        [ SA.cx "16"
                        , SA.cy "21"
                        , SA.rx "4"
                        , SA.ry "4"
                        , SA.strokeWidth "3"
                        , SA.stroke gradientUrl
                        , SA.fill gradientUrl
                        , SA.transform ""
                        , SA.id "ellipse2"
                        ]
                        []
                    , Svg.ellipse
                        [ SA.cx "10"
                        , SA.cy "23"
                        , SA.rx "3"
                        , SA.ry "3"
                        , SA.strokeWidth "3"
                        , SA.stroke gradientUrl
                        , SA.fill gradientUrl
                        , SA.transform ""
                        , SA.id "ellipse3"
                        ]
                        []
                    , Svg.ellipse
                        [ SA.cx "26"
                        , SA.cy "23"
                        , SA.rx "3"
                        , SA.ry "3"
                        , SA.strokeWidth "3"
                        , SA.stroke gradientUrl
                        , SA.fill gradientUrl
                        , SA.transform ""
                        , SA.id "ellipse4"
                        ]
                        []
                    , Svg.rect
                        [ SA.x "12"
                        , SA.y "25"
                        , SA.width "13"
                        , SA.height "1"
                        , SA.strokeWidth "3"
                        , SA.stroke gradientUrl
                        , SA.fill gradientUrl
                        , SA.transform ""
                        , SA.id "rect2"
                        ]
                        []
                    ]
                ]
            ]
        , Svg.linearGradient
            [ SA.id gradientId
            , SA.gradientUnits "userSpaceOnUse"
            , SA.x1 "231.8955"
            , SA.y1 "-269.5547"
            , SA.x2 "231.8955"
            , SA.y2 "-301.7199"
            , SA.gradientTransform "matrix(1 0 0 -1 -213.5 -268.5)"
            , SA.type_ "simple"
            ]
            [ Svg.stop
                [ SA.offset "0"
                , SA.style ("stop-color:" ++ colorTop)
                ]
                []
            , Svg.stop
                [ SA.offset "1"
                , SA.style ("stop-color:" ++ colorBottom)
                ]
                []
            ]
        ]
