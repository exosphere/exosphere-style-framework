module FrameworkCustomized exposing (initConf, main, view)

import Browser
import Color
import Element exposing (alignRight, alpha, column, el, link, moveLeft, none, paddingEach, paragraph, text)
import Element.Font as Font
import Framework
import FrameworkCustomized.Icon
import FrameworkCustomized.Logo as Logo
import Html


main : Program Framework.Flags Framework.Model Framework.Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = Framework.update
        , subscriptions = Framework.subscriptions
        }


view : Framework.Model -> Html.Html Framework.Msg
view model =
    Framework.view
        { model
            | conf = initConf
            , introspections = introspections
        }


init : Framework.Flags -> ( Framework.Model, Cmd msg )
init flags =
    let
        initModel =
            Framework.initModel flags
    in
    ( { initModel
        | conf = initConf
        , introspections = introspections
      }
    , Framework.initCmd
    )


introspections : List ( Framework.Introspection, Bool )
introspections =
    [ ( Logo.introspection, True )
    , ( FrameworkCustomized.Icon.introspection, True )
    ]
        ++ Framework.introspections


initConf : Framework.Conf msg
initConf =
    let
        confData =
            Framework.initConf
    in
    { confData
        | titleLeftSide =
            column []
                [ link []
                    { label =
                        el
                            [ alpha 0.3
                            , paddingEach
                                { top = 0
                                , right = 0
                                , bottom = 20
                                , left = 0
                                }
                            ]
                        <|
                            Logo.logo (Logo.LogoExosphere Logo.Monochrome) 60
                    , url = ".."
                    }
                , paragraph
                    [ Font.size 45
                    , Font.bold
                    , moveLeft 3
                    ]
                    [ el [ alpha 0.5 ] <| text "exosphere"
                    ]
                ]
        , title =
            column []
                [ link []
                    { label =
                        el
                            [ alpha 0.8
                            , paddingEach
                                { top = 0
                                , right = 0
                                , bottom = 20
                                , left = 0
                                }
                            ]
                        <|
                            Logo.logo (Logo.LogoExosphere Logo.Default) 60
                    , url = ".."
                    }
                , paragraph
                    [ Font.size 55
                    , Font.bold
                    , moveLeft 3
                    ]
                    [ el [ alpha 0.5 ] <| text "exosphere"
                    ]
                ]
        , subTitle = "LIVING STYLE GUIDE"
        , version = "0.0.1"
        , introduction = none
        , mainPadding = 41
        , password = ""
        , forkMe =
            Element.inFront <|
                link
                    [ alignRight
                    , Font.color <| Color.toElementColor Color.black
                    ]
                    { label = Element.text "Fork Me"
                    , url = "https://gitlab.com/exosphere/exosphere"
                    }
        , hostnamesWithoutPassword = \hostname -> hostname == "localhost"
    }
