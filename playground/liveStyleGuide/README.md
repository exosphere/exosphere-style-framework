# Live Style Guide

Experiment quickly with a live-reloading view of the customized style framework

## Option 1: Run using convenience script in package.json

In the root of the project (where `package.json` is):

```bash
git submodule sync --recursive
git submodule update --init --recursive
npm install
npm run live-style-guide
```

## Option 2: Run `elm-live` manually 

### Install project dependencies

In the root of project (where `package.json` is):

```bash
git submodule sync --recursive
git submodule update --init --recursive
npm install
npm install -g npx
```

### Use `elm-live` to run the live style guide

```bash
cd playground/liveStyleGuide/
npx elm-live ../../src/FrameworkCustomized.elm --open --dir=. -- --output=elm.js --debug
```

While `elm-live` is running and you modify and save any of the Elm files the style guide depends on, then the browser 
should refresh automatically.
