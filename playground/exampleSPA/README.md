# An Example SPA

Non-trivial layouts using multiple widgets from the customized style framework

## Install project dependencies

In the root of project (where `package.json` is):

```bash
git submodule sync --recursive
git submodule update --init --recursive
npm install
npm install -g npx
```

## Use `elm-live` to run the example

```bash
cd playground/exampleSPA/
npx elm-live src/ExampleSPA.elm --open --dir=. -- --output=elm.js --debug
```

While `elm-live` is running and you modify and save any of the Elm files the example depends on, then the browser 
should refresh automatically.
